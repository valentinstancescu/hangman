<?php

namespace VStancescu\Hangman\Api;

/**
 * Class ApiResponseErrors - This class is responsible with holding a mapping of error codes and messages which are
 * returned from all the APIs.
 */
class ApiResponseErrors
{
    //Game errors
    const GAME_MISSING_ID = 40001;
    const GAME_MISSING_LETTER = 40002;
    const GAME_NOT_FOUND = 40003;
    const GAME_FINISHED = 40004;

    // API Error Handler errors
    const APPLICATION_INTERNAL_ERROR    = 50500;
    const APPLICATION_PHP_ERROR         = 50501;
    const APPLICATION_NOT_FOUND_ERROR   = 40404;
    const APPLICATION_NOT_ALLOWED_ERROR = 40405;

    const ERRORS = [

        //Game errors
        self::GAME_MISSING_ID => 'Game id is missing or invalid on request',
        self::GAME_MISSING_LETTER => 'Game letter is missing or invalid on request',
        self::GAME_NOT_FOUND => 'Game not found for selected id',
        self::GAME_FINISHED => 'Game cannot be played anymore',

        // API Error Handler errors
        self::APPLICATION_INTERNAL_ERROR => 'Application error',
        self::APPLICATION_PHP_ERROR => 'Internal application error',
        self::APPLICATION_NOT_FOUND_ERROR => 'Not found',
        self::APPLICATION_NOT_ALLOWED_ERROR => 'Method not allowed. HTTP method must be: %s',
    ];


}
