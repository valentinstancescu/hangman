<?php

namespace VStancescu\Hangman\Api;

/**
 * Class Response - Value object class that describes the API response
 */
class Response
{
    /** @var integer */
    private $gameId;

    /** @var string */
    private $word;

    /** @var string */
    private $status;

    /** @var integer */
    private $triesLeft;

    /**
     * Response constructor.
     * @param $gameId
     * @param $word
     * @param $status
     * @param $triesLeft
     */
    public function __construct($gameId, $word, $status, $triesLeft)
    {
        $this->gameId = $gameId;
        $this->word = $word;
        $this->status = $status;
        $this->triesLeft = $triesLeft;
    }

    /**
     * @return int
     */
    public function getGameId(): int
    {
        return $this->gameId;
    }

    /**
     * @return string
     */
    public function getWord(): string
    {
        return $this->word;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getTriesLeft(): int
    {
        return $this->triesLeft;
    }
}
