<?php

namespace VStancescu\Hangman\Api;

/**
 * Class ApiJsonResponseBuilder - this class build and format the API response in the desired format
 */
class ApiJsonResponseBuilder
{

    /**
     * @param Response $response
     *
     * @return \Slim\Http\Response
     */
    public function successResponse(Response $response): \Slim\Http\Response
    {

        return (new \Slim\Http\Response())
            ->withJson(
                [
                    'game_id' => $response->getGameId(),
                    'tries_left' => $response->getTriesLeft(),
                    'status' => strtolower($response->getStatus()),
                    'word' => $response->getWord(),
                ]
            );
    }


    /**
     * @param int $httpStatus
     * @param string $errorCode
     * @param string $errorMessage
     *
     * @return \Slim\Http\Response
     */
    public function errorResponse($httpStatus, $errorCode, $errorMessage): \Slim\Http\Response
    {
        return (new \Slim\Http\Response($httpStatus))
            ->withJson(
                [
                    'error' => [
                        'code' => (string)$errorCode,
                        'message' => $errorMessage,
                    ]
                ]
            );

    }

}