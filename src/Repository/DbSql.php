<?php

namespace VStancescu\Hangman\Repository;

use PDO;
use PDOException;
use VStancescu\Hangman\Common\Config;

/**
 * Class DbSql - handles database connection and offers basic database operations via PDO
 */
class DbSql{

    /** @var PDO */
    private $dbHandler;

    /** @var string */
    private $error;

    /** @var \PDOStatement */
    private $statement;

    /**
     * DbSql constructor.
     * @param Config $dbConfig
     */
    public function __construct(Config $dbConfig)
    {

        $dbConnection = $dbConfig->get('connection');

        //dsn for mysql
        $dsn = "mysql:host=" . $dbConnection['host'] . ";dbname=" . $dbConnection['database'] . ";charset=utf8";
        $options = [
            PDO::ATTR_PERSISTENT    => true,
            PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION,
        ];

        try{
            $this->dbHandler = new PDO($dsn, $dbConnection['user'], $dbConnection['password'], $options);
        } catch (PDOException $e){
            $this->error = $e->getMessage();
        }

    }

    /**
     * @param string $query
     */
    public function query($query){
        $this->statement = $this->dbHandler->prepare($query);
    }

    /**
     * @param string $param
     * @param mixed $value
     * @param mixed|null $type
     */
    public function bind($param, $value, $type = null){
        if(is_null($type)){
            switch (true){
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->statement->bindValue($param, $value, $type);
    }

    /**
     * @return bool
     */
    public function execute(){
        return $this->statement->execute();
    }

    /**
     * @return array
     */
    public function getAll(){
        $this->execute();
        return $this->statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return mixed
     */
    public function getRow(){
        $this->execute();
        return $this->statement->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @return int
     */
    public function rowCount(){
        return $this->statement->rowCount();
    }

    /**
     * @return string
     */
    public function lastInsertId(){
        return $this->dbHandler->lastInsertId();
    }

    /**
     * @param string $param
     * @param int $type
     * @return string
     */
    public function quote($param, $type = PDO::PARAM_STR)
    {
        return$this->dbHandler->quote($param, $type);
    }

}
