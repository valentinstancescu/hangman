<?php

namespace VStancescu\Hangman\Repository;

use PDO;
use VStancescu\Hangman\Entity\Game;
use VStancescu\Hangman\Entity\GameTry;
use VStancescu\Hangman\Entity\Word;
use VStancescu\Hangman\Exception\HangmanNotFoundException;

/**
 * Class HangmanRepository - Repository class that interacts with db in order to provide different information
 */
class HangmanRepository
{

    /** @var DbSql */
    private $dbHandler;

    /**
     * HangmanRepository constructor.
     * @param DbSql $dbHandler
     */
    public function __construct(DbSql $dbHandler)
    {
        $this->dbHandler = $dbHandler;
    }

    /**
     * @return Word
     */
    public function getWordForGame(): Word
    {
        $sql = "SELECT `words`.`word_id`, `words`.`word` FROM `words` ORDER BY RAND() LIMIT 1";

        $this->dbHandler->query($sql);
        $returnedRow = $this->dbHandler->getRow();

        return new Word($returnedRow['word_id'], $returnedRow['word']);
    }

    /**
     * @param Game $game
     *
     * @return Game
     */
    public function saveGame(Game $game): Game
    {
        $sql = "INSERT INTO `games` (`word_id`, `status`, `tries_left`) VALUES (:wordId, :status, :triesLeft)";

        $this->dbHandler->query($sql);
        $this->dbHandler->bind(':wordId', $game->getWord()->getWordId(), PDO::PARAM_INT);
        $this->dbHandler->bind(':status', $game->getStatus(), PDO::PARAM_STR);
        $this->dbHandler->bind(':triesLeft', $game->getTriesLeft(), PDO::PARAM_INT);

        $this->dbHandler->execute();

        $gameId = $this->dbHandler->lastInsertId();

        return $game->withGameId($gameId);
    }

    /**
     * @param int $id
     *
     * @return Game
     * @throws HangmanNotFoundException
     */
    public function getGameById(int $id): Game
    {
        $sql = "SELECT `games`.`game_id`, `games`.`status`, `games`.`tries_left`, `words`.`word_id`, `words`.`word` 
                  FROM `games` 
                  JOIN `words` ON `games`.`word_id` = `words`.`word_id`
                  WHERE `games`.`game_id` = :gameId
                  ";
        $this->dbHandler->query($sql);
        $this->dbHandler->bind(':gameId', $id, PDO::PARAM_INT);

        $returnedRow = $this->dbHandler->getRow();

        if (!$returnedRow) {
            throw new HangmanNotFoundException(sprintf('GameId %d not found!', $id));
        }

        $gameWord = new Word($returnedRow['word_id'], $returnedRow['word']);

        return (new Game($gameWord))
            ->withGameId($returnedRow['game_id'])
            ->withtStatus($returnedRow['status'])
            ->withTriesLeft($returnedRow['tries_left']);
    }

    /**
     * @param int $gameId
     *
     * @return array
     */
    public function getLettersUsedForGameId(int $gameId): array
    {
        $sql = "SELECT * FROM `game_tries` WHERE `game_tries`.`game_id` = :gameId";
        $this->dbHandler->query($sql);
        $this->dbHandler->bind(':gameId', $gameId, PDO::PARAM_INT);

        $returnedLetters = $this->dbHandler->getAll();

        if (empty($returnedLetters)) {

            return [];
        } else {

            $alreadyGuessedLetters = [];
            foreach ($returnedLetters as $returnedLetter) {
                $alreadyGuessedLetters[] = new GameTry($returnedLetter['game_id'], $returnedLetter['letter']);
            }

            return $alreadyGuessedLetters;
        }
    }

    /**
     * @param GameTry $gameTry
     *
     * @return void
     */
    public function saveGameTry(GameTry $gameTry)
    {
        $sql = "INSERT INTO `game_tries` (`game_id`, `letter`) VALUES (:gameId, :letter)";

        $this->dbHandler->query($sql);
        $this->dbHandler->bind(':gameId', $gameTry->getGameId(), PDO::PARAM_INT);
        $this->dbHandler->bind(':letter', strtolower($gameTry->getGuessedLetter()), PDO::PARAM_STR);

        $this->dbHandler->execute();
    }

    /**
     * @param Game $game
     * @param int $triesLeft
     *
     * @return void
     */
    public function updateGameTries(Game $game, $triesLeft)
    {
        $sql = "UPDATE `hangman`.`games` SET `tries_left` = :triesLeft WHERE `game_id` = :gameId";

        $this->dbHandler->query($sql);
        $this->dbHandler->bind(':gameId', $game->getGameId(), PDO::PARAM_INT);
        $this->dbHandler->bind(':triesLeft', (int)$triesLeft, PDO::PARAM_INT);

        $this->dbHandler->execute();
    }

    /**
     * @param Game $game
     * @param string $status
     *
     * @return void
     */
    public function updateGameStatus(Game $game, $status)
    {
        $sql = "UPDATE `hangman`.`games` SET `status` = :status WHERE `game_id` = :gameId";

        $this->dbHandler->query($sql);
        $this->dbHandler->bind(':gameId', $game->getGameId(), PDO::PARAM_INT);
        $this->dbHandler->bind(':status', $status, PDO::PARAM_STR);

        $this->dbHandler->execute();
    }
}
