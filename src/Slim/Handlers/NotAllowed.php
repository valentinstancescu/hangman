<?php

namespace VStancescu\Hangman\Slim\Handlers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Handlers\AbstractHandler;
use VStancescu\Hangman\Api\ApiJsonResponseBuilder;
use VStancescu\Hangman\Api\ApiResponseErrors;

class NotAllowed extends AbstractHandler
{
    /** @var ApiJsonResponseBuilder */
    private $responseBuilder;

    /**
     * NotAllowed constructor.
     * @param ApiJsonResponseBuilder $responseBuilder
     */
    public function __construct(ApiJsonResponseBuilder $responseBuilder)
    {
        $this->responseBuilder = $responseBuilder;
    }


    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param array $methods
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $methods)
    {
        $allow = implode(', ', $methods);

        return $this->responseBuilder->errorResponse(
            405,
            ApiResponseErrors::APPLICATION_NOT_ALLOWED_ERROR,
            sprintf(ApiResponseErrors::ERRORS[ApiResponseErrors::APPLICATION_NOT_ALLOWED_ERROR], $allow)
        );
    }
}
