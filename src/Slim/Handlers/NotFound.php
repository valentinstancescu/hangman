<?php

namespace VStancescu\Hangman\Slim\Handlers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Handlers\AbstractHandler;
use VStancescu\Hangman\Api\ApiJsonResponseBuilder;
use VStancescu\Hangman\Api\ApiResponseErrors;

class NotFound extends AbstractHandler
{
    /** @var ApiJsonResponseBuilder */
    private $responseBuilder;

    /**
     * NotAllowed constructor.
     * @param ApiJsonResponseBuilder $responseBuilder
     */
    public function __construct(ApiJsonResponseBuilder $responseBuilder)
    {
        $this->responseBuilder = $responseBuilder;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response)
    {
        return $this->responseBuilder->errorResponse(
            404,
            ApiResponseErrors::APPLICATION_NOT_FOUND_ERROR,
            ApiResponseErrors::ERRORS[ApiResponseErrors::APPLICATION_NOT_FOUND_ERROR]
        );
    }
}
