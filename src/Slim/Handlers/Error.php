<?php

namespace VStancescu\Hangman\Slim\Handlers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Handlers\AbstractHandler;
use VStancescu\Hangman\Api\ApiJsonResponseBuilder;
use VStancescu\Hangman\Api\ApiResponseErrors;

class Error extends AbstractHandler
{
    /** @var ApiJsonResponseBuilder */
    private $responseBuilder;

    /**
     * NotAllowed constructor.
     * @param ApiJsonResponseBuilder $responseBuilder
     */
    public function __construct(ApiJsonResponseBuilder $responseBuilder)
    {
        $this->responseBuilder = $responseBuilder;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param \Exception $exception
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, \Exception $exception)
    {
        $apiResponse = $this->responseBuilder->errorResponse(
            500,
            ApiResponseErrors::APPLICATION_INTERNAL_ERROR,
            ApiResponseErrors::ERRORS[ApiResponseErrors::APPLICATION_INTERNAL_ERROR]
        );

        return $apiResponse;
    }
}
