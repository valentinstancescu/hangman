<?php

namespace VStancescu\Hangman\Slim\Handlers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Handlers\AbstractHandler;
use VStancescu\Hangman\Api\ApiJsonResponseBuilder;
use VStancescu\Hangman\Api\ApiResponseErrors;

class PhpError extends AbstractHandler
{

    /** @var ApiJsonResponseBuilder */
    private $responseBuilder;

    /**
     * NotAllowed constructor.
     * @param ApiJsonResponseBuilder $responseBuilder
     */
    public function __construct(ApiJsonResponseBuilder $responseBuilder)
    {
        $this->responseBuilder = $responseBuilder;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param \Throwable $error
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, \Throwable $error)
    {
        return $this->responseBuilder->errorResponse(
            500,
            ApiResponseErrors::APPLICATION_PHP_ERROR,
            ApiResponseErrors::ERRORS[ApiResponseErrors::APPLICATION_PHP_ERROR]
        );
    }
}
