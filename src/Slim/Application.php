<?php

namespace VStancescu\Hangman\Slim;


use DI\Bridge\Slim\App;
use DI\ContainerBuilder;

class Application extends App
{
    protected function configureContainer(ContainerBuilder $builder)
    {

        $builder->addDefinitions(dirname(__DIR__) . '/../config/slim-config.php');
        $builder->addDefinitions(dirname(dirname(__DIR__)) . '/config/db-config.php');
    }
}