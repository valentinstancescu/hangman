<?php

namespace VStancescu\Hangman\Slim;


use VStancescu\Hangman\Controller\HangmanController;

class ApplicationBuilder
{
    /**
     * @return Application
     */
    public function createApplication()
    {
        //define API routes
        $app = new Application();
        $app->group('/game', function () {
            /**@var Application $this */
            $this->post('/new', [HangmanController::class, 'newGame']);
            /**@var Application $this */
            $this->post('/{id}/{letter}', [HangmanController::class, 'checkLetterForGameId']);
            /**@var Application $this */
            $this->get('/{id}', [HangmanController::class, 'getGameById']);
        });

        return $app;
    }
}
