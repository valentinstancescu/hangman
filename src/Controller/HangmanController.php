<?php

namespace VStancescu\Hangman\Controller;

use VStancescu\Hangman\Api\ApiJsonResponseBuilder;
use VStancescu\Hangman\Api\ApiResponseErrors;
use VStancescu\Hangman\Api\Response;
use VStancescu\Hangman\Entity\Game;
use VStancescu\Hangman\Entity\GameTry;
use VStancescu\Hangman\Exception\HangmanServiceException;
use VStancescu\Hangman\Service\HangmanService;

/**
 * Class HangmanController - class that provides methods used in the API
 *
 */
class HangmanController
{
    /** @var  HangmanService */
    private $hangmanService;

    /** @var ApiJsonResponseBuilder */
    private $responseBuilder;

    /**
     * HangmanController constructor.
     * @param HangmanService $hangmanService
     * @param ApiJsonResponseBuilder $responseBuilder
     */
    public function __construct(HangmanService $hangmanService, ApiJsonResponseBuilder $responseBuilder)
    {
        $this->hangmanService = $hangmanService;
        $this->responseBuilder = $responseBuilder;
    }

    /**
     * Creates new hangman game and returns formatted API response
     *
     * @return \Slim\Http\Response
     */
    public function newGame()
    {
        $generatedGame = $this->hangmanService->prepareNewGame();

        $response = new Response(
            $generatedGame->getGameId(),
            $this->hangmanService->parseWord($generatedGame),
            $generatedGame->getStatus(),
            $generatedGame->getTriesLeft()
        );

        return $this->responseBuilder->successResponse($response);
    }

    /**
     * Checks a letter in a game and returns formatted API response
     *
     * @param string $id
     * @param string $letter
     *
     * @return \Slim\Http\Response
     */
    public function checkLetterForGameId($id, $letter)
    {
        //validate request parameters
        if (empty($id) || !is_numeric($id)) {

            return $this->responseBuilder->errorResponse(
                400,
                ApiResponseErrors::GAME_MISSING_ID,
                ApiResponseErrors::ERRORS[ApiResponseErrors::GAME_MISSING_ID]
            );
        }

        if (empty($letter) || iconv_strlen($letter) > 1) {

            return $this->responseBuilder->errorResponse(
                400,
                ApiResponseErrors::GAME_MISSING_LETTER,
                ApiResponseErrors::ERRORS[ApiResponseErrors::GAME_MISSING_LETTER]
            );
        }


        try {
            //load game by id
            $currentGame = $this->hangmanService->getGameById((int)$id);

            //if game is not playable anymore return error response
            if ($currentGame->getStatus() != Game::GAME_STATUS_BUSY) {
                return $this->responseBuilder->errorResponse(
                    400,
                    ApiResponseErrors::GAME_FINISHED,
                    ApiResponseErrors::ERRORS[ApiResponseErrors::GAME_FINISHED]
                );
            }

            //check letter provided
            $isLetterUsedAlreadyForGame = $this->hangmanService->isLetterUsedForGame($currentGame, $letter);

            if (!$isLetterUsedAlreadyForGame) {

                $gameWord = strtolower($currentGame->getWord()->getWord());

                $isLetterInWord = (bool) substr_count($gameWord, $letter);
                if (!$isLetterInWord) {
                    $leftTries = $this->hangmanService->decreaseGameTries($currentGame);

                    if ($leftTries === 0) {
                        $this->hangmanService->updateGameStatus($currentGame, Game::GAME_STATUS_FAILED);
                    }
                }

                $gameTry = new GameTry($currentGame->getGameId(), $letter);
                $this->hangmanService->saveGameTry($gameTry);
            }

            //parse word to display it properly
            $wordToBeDisplayed = $this->hangmanService->parseWord($currentGame);

            //check if word has been completly discovered
            if ($wordToBeDisplayed === $currentGame->getWord()->getWord()) {
                $this->hangmanService->updateGameStatus($currentGame, Game::GAME_STATUS_SUCCESS);
            }

            //this is not optimal approach but was faster due to time constraint
            $newGameReloaded = $this->hangmanService->getGameById((int)$id);

            //return response
            $response = new Response(
                $newGameReloaded->getGameId(),
                $wordToBeDisplayed,
                $newGameReloaded->getStatus(),
                $newGameReloaded->getTriesLeft()
            );

            return $this->responseBuilder->successResponse($response);

        } catch (HangmanServiceException $exception) {

            return $this->responseBuilder->errorResponse(
                400,
                ApiResponseErrors::GAME_NOT_FOUND,
                ApiResponseErrors::ERRORS[ApiResponseErrors::GAME_NOT_FOUND]
            );
        }
    }

    /**
     * Displays data about a game and returns formatted API response
     *
     * @param string $id
     *
     * @return \Slim\Http\Response
     */
    public function getGameById($id)
    {
        try {
            $currentGame = $this->hangmanService->getGameById((int)$id);

            $wordToBeDisplayed = $this->hangmanService->parseWord($currentGame);

            $response = new Response(
                $currentGame->getGameId(),
                $wordToBeDisplayed,
                $currentGame->getStatus(),
                $currentGame->getTriesLeft()
            );

            return $this->responseBuilder->successResponse($response);

        } catch (HangmanServiceException $exception) {

            return $this->responseBuilder->errorResponse(
                400,
                ApiResponseErrors::GAME_NOT_FOUND,
                ApiResponseErrors::ERRORS[ApiResponseErrors::GAME_NOT_FOUND]
            );
        }
    }
}
