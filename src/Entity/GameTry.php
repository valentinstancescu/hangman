<?php

namespace VStancescu\Hangman\Entity;


class GameTry
{
    /** @var integer */
    private $gameTryId;

    /** @var integer */
    private $gameId;

    /** @var string */
    private $guessedLetter;

    /**
     * GameTry constructor.
     * @param int $gameId
     * @param string $guessedLetter
     */
    public function __construct(int $gameId, string $guessedLetter)
    {
        $this->gameId = $gameId;
        $this->guessedLetter = $guessedLetter;
    }

    /**
     * @return int
     */
    public function getGameTryId(): int
    {
        return $this->gameTryId;
    }

    /**
     * @return int
     */
    public function getGameId(): int
    {
        return $this->gameId;
    }

    /**
     * @return string
     */
    public function getGuessedLetter(): string
    {
        return $this->guessedLetter;
    }
}