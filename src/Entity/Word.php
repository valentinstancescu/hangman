<?php

namespace VStancescu\Hangman\Entity;


class Word
{
    /** @var integer */
    private $wordId;

    /** @var string */
    private $word;

    /**
     * Word constructor.
     * @param int $wordId
     * @param string $word
     */
    public function __construct(int $wordId, string $word)
    {
        $this->wordId = $wordId;
        $this->word = $word;
    }

    /**
     * @return int
     */
    public function getWordId(): int
    {
        return $this->wordId;
    }

    /**
     * @return string
     */
    public function getWord(): string
    {
        return $this->word;
    }
}
