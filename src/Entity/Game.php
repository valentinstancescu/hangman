<?php

namespace VStancescu\Hangman\Entity;


class Game
{
    const GAME_STATUS_BUSY = 'BUSY';
    const GAME_STATUS_FAILED = 'FAIL';
    const GAME_STATUS_SUCCESS = 'SUCCESS';

    /** @var integer */
    private $gameId;

    /** @var Word */
    private $word;

    /** @var string */
    private $status = self::GAME_STATUS_BUSY;

    /** @var integer */
    private $triesLeft = 6;

    /**
     * Game constructor.
     * @param Word $word
     */
    public function __construct(Word $word)
    {
        $this->word = $word;
    }

    /**
     * @return int
     */
    public function getGameId(): int
    {
        return $this->gameId;
    }

    /**
     * @return Word
     */
    public function getWord(): Word
    {
        return $this->word;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getTriesLeft(): int
    {
        return $this->triesLeft;
    }

    /**
     * @param int $triesLeft
     *
     * @return Game
     */
    public function withTriesLeft(int $triesLeft)
    {
        $this->triesLeft = $triesLeft;

        return $this;
    }

    /**
     * @param int $gameId
     *
     * @return $this
     */
    public function withGameId(int $gameId): Game
    {
        $this->gameId = $gameId;

        return $this;
    }

    /**
     * @param string $status
     *
     * @return Game
     */
    public function withtStatus(string $status): Game
    {
        $this->status = $status;

        return $this;
    }
}
