<?php

namespace VStancescu\Hangman\Common;


use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

/**
 * Class Config - config parser that helps to get configuration settings to use in the application
 */
class Config
{
    /** @var ContainerInterface */
    private $container;

    /** @param ContainerInterface $container */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function get($name)
    {
        try {
            return $this->container->get(sprintf('settings.%s', $name));
        } catch (NotFoundExceptionInterface $notFoundException) {
            return null;
        }
    }
}
