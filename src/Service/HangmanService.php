<?php

namespace VStancescu\Hangman\Service;

use VStancescu\Hangman\Entity\Game;
use VStancescu\Hangman\Entity\GameTry;
use VStancescu\Hangman\Exception\HangmanNotFoundException;
use VStancescu\Hangman\Exception\HangmanServiceException;
use VStancescu\Hangman\Repository\HangmanRepository;

/**
 * Class HangmanService - class that provides methods that handles a game
 */
class HangmanService
{
    /** @var HangmanRepository */
    private $hangmanRepository;

    /**
     * HangmanService constructor.
     * @param HangmanRepository $hangmanRepository
     */
    public function __construct(HangmanRepository $hangmanRepository)
    {
        $this->hangmanRepository = $hangmanRepository;
    }

    /**
     * @return Game
     */
    public function prepareNewGame(): Game
    {
        $wordForGame = $this->hangmanRepository->getWordForGame();
        $newGame = $this->hangmanRepository->saveGame(new Game($wordForGame));

        return $newGame;
    }

    /**
     * @param int $id
     *
     * @return Game
     * @throws HangmanServiceException
     */
    public function getGameById($id): Game
    {
        try {
            return $this->hangmanRepository->getGameById($id);
        } catch (HangmanNotFoundException $exception) {
            throw new HangmanServiceException($exception->getMessage());
        }
    }

    /**
     * @param Game $game
     * @param $letter
     *
     * @return bool
     */
    public function isLetterUsedForGame(Game $game, $letter): bool
    {
        $lettersAlreadyUsedForGame = $this->hangmanRepository->getLettersUsedForGameId($game->getGameId());

        /** @var GameTry $gameTry */
        foreach ($lettersAlreadyUsedForGame as $gameTry) {
            if ($gameTry->getGuessedLetter() === $letter) {

                return true;
            }
        }

        return false;
    }

    /**
     * @param GameTry $gameTry
     *
     * @return void
     */
    public function saveGameTry(GameTry $gameTry)
    {
        $this->hangmanRepository->saveGameTry($gameTry);

        return;
    }


    /**
     * @param Game $game
     *
     * @return string
     */
    public function parseWord(Game $game): string
    {
        $lettersAlreadyUsedForGame = $this->hangmanRepository->getLettersUsedForGameId($game->getGameId());
        $gameWord = $game->getWord()->getWord();

        $guessedLetters = [];
        /** @var GameTry $letterFromGame */
        foreach ($lettersAlreadyUsedForGame as $letterFromGame) {
            $guessedLetters[] = $letterFromGame->getGuessedLetter();
        }

        $wordToBeDisplayed = '';
        for ($i = 0; $i < iconv_strlen($gameWord); $i++) {

            $letter = $gameWord[$i];

            if (in_array($letter, $guessedLetters)) {
                $wordToBeDisplayed .= $letter;
            } else {
                $wordToBeDisplayed .= '.';
            }
        }

        return  $wordToBeDisplayed;
    }

    /**
     * @param Game $game
     *
     * @return int
     */
    public function decreaseGameTries(Game $game): int
    {
        $currentGameTriesLeft = $game->getTriesLeft();
        $triesLeft = (int) $currentGameTriesLeft - 1;

        $this->hangmanRepository->updateGameTries($game, $triesLeft);

        return $triesLeft;
    }

    /**
     * @param Game $game
     * @param $status
     *
     * @return void
     */
    public function updateGameStatus(Game $game, $status)
    {
        $this->hangmanRepository->updateGameStatus($game, $status);
    }
}
