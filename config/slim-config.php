<?php

use VStancescu\Hangman\Slim\Handlers\NotAllowed;
use VStancescu\Hangman\Slim\Handlers\NotFound;
use VStancescu\Hangman\Slim\Handlers\Error;
use VStancescu\Hangman\Slim\Handlers\PhpError;

return [
    'settings.displayErrorDetails' => true,
    'notFoundHandler' => \DI\get(NotFound::class),
    'notAllowedHandler' => \DI\get(NotAllowed::class),
    'errorHandler' => \DI\get(Error::class),
    'phpErrorHandler' => \DI\get(PhpError::class),
];