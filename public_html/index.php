<?php
//endpoint file for the API
use VStancescu\Hangman\Slim\ApplicationBuilder;

require_once(dirname(__FILE__) . '/../app/init.php');

$appBuilder = new ApplicationBuilder();
$app = $appBuilder->createApplication();
$app->run();
