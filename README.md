##Hangman game in PHP through REST API communication

A simple application implementing a minimal REST (or RESTful) API for playing a game of hangman.

### API endpoints

```
1. [POST] /game/new              - create a new game
2. [GET]  /game/:id              - retrieve existing game data
3. [POST] /game/:id/:char        - guessed character for game with id :id
```

All endpoints return the same JSON game-data structure (assuming no errors were encountered).

#### API parameters

|Name|Type|Description|
|:-------------|:-------------|:---|
|id|integer|game_id from api response|
|char|string|a single, lowercase letter between `a` and `z`|


#### API response data

All endpoints return the following JSON data structure (assuming no errors);

```json
{
    "game_id"     : 1,
    "tries_left"  : 11,
    "status"      : "busy",
    "word"        : "...."
}
```

Example of an error message

```json
{
    "error"      : {
        "code"   : 400,
        "message": "Bad Request"
    }
}
```


---

## Installation of the application

1. Clone project from Bitbucket: `git clone https://valentinstancescu@bitbucket.org/valentinstancescu/hangman.git`
2. Make sure the server where project is deployed runs: `PHP >=7.0`, `Apache2`, `MySql >= 5.5`.
3. Php modules that must to be enabled: `PDO`.
4. Application also requires `COMPOSER` to be installed.
5. Copy files on the server, making `public_html` folder as root of the project.
6. Run `composer install` to install external libraries as _Slim_ and _PHP_DI_
7. Create database tables:
   ``CREATE TABLE `game_tries` (
       `game_tries_id` INT(11) NOT NULL AUTO_INCREMENT,
       `game_id` INT(11) NOT NULL,
       `letter` VARCHAR(1) NOT NULL,
       PRIMARY KEY (`game_tries_id`),
       KEY `game_id` (`game_id`),
       KEY `letter` (`letter`)
     ) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
     
     CREATE TABLE `games` (
       `game_id` INT(11) NOT NULL AUTO_INCREMENT,
       `word_id` INT(11) NOT NULL,
       `status` ENUM('BUSY','FAIL','SUCCESS') NOT NULL DEFAULT 'BUSY',
       `tries_left` INT(1) NOT NULL DEFAULT '6',
       PRIMARY KEY (`game_id`)
     ) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
     
     CREATE TABLE `words` (
       `word_id` INT(11) NOT NULL AUTO_INCREMENT,
       `word` VARCHAR(50) NOT NULL,
       PRIMARY KEY (`word_id`)
     ) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8``
8. Configure file `\hangman\config\db-config.php` with according data from your database
---

## Folders/files and their roles

Short description of folders and files of the project

1. `\app\init.php` - holds the files that initialize the common data for all application.
2. `\config` - holds configuration files.
3. `\public_html` - holds entry points / public files of the project.
6. `\src` - holds sources of the application grouped on specific namespaces
7. `\composer.json` - Composer file where external libraries are specified.

- External libraries used: _Slim_ and _PHP_DI_